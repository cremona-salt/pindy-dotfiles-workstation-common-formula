# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for store, data in pindy_dotfiles_workstation_common.pass.get('pass_stores', {}).items() %}
pindy-dotfiles-workstation-common-pass-config-repo-{{ store }}-pass-cloned:
  git.latest:
    - name: {{ data.repo }}
    - target: {{ data.target }}
    - user: pindy
    {%- if data.repo != 'https://github.com/saltstack-formulas/template-formula.git' %}
    {# This is to allow tests to pass without access to a real ssh key #}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}

pindy-dotfiles-workstation-common-pass-config-repo-{{ store }}-pass-group-managed:
  file.directory:
    - name: {{ data.target }}
    - user: pindy
    - group: pindy
    - recurse:
      - user
      - group
{%- endfor %}

pindy-dotfiles-workstation-common-pass-config-repo-qtpass-config-dir-managed:
  file.directory:
    - name: /home/pindy/.config/IJHack
    - mode: 700
    - user: pindy
    - group: pindy
    - makedirs: True

pindy-dotfiles-workstation-common-pass-config-repo-qtpass-config-file-managed:
  file.managed:
    - name: /home/pindy/.config/IJHack/QtPass.conf
    - mode: 600
    - user: pindy
    - group: pindy
    - makedirs: True
    - replace: False

pindy-dotfiles-workstation-common-pass-config-repo-qtpass-config-content-managed:
  ini.options_present:
    - name: /home/pindy/.config/IJHack/QtPass.conf
    - sections:
        General:
          addGPGid: true
          alwaysOnTop: false
          autoPull: true
          autoPush: true
          autoclearPanelSeconds: 10
          autoclearSeconds: 45
          avoidCapitals: false
          avoidNumbers: false
          clipBoardType: 2
          gitExecutable: /usr/bin/git
          gpgExecutable: /usr/bin/gpg2
          hideContent: false
          hideOnClose: false
          hidePassword: false
          lessRandom: false
          passExecutable: /usr/bin/pass
          passStore: /home/pindy/.password-store/
          passTemplate: login\nurl
          passwordChars: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_-+: {}[]|:;<>,.?"
          passwordCharsselection: 0
          passwordLength: 16
          pwgenExecutable: /usr/bin/pwgen
          startMinimized: false
          templateAllFields: false
          useAutoclear: true
          useAutoclearPanel: false
          useGit: true
          useOtp: false
          usePass: true
          usePwgen: true
          useQrencode: false
          useSelection: true
          useSymbols: false
          useTemplate: false
          useTrayIcon: true
