# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-gdm-config-file-accountsservice-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

pindy-dotfiles-workstation-common-gdm-config-file-accountsservice-users-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService/users
    - user: root
    - group: root
    - mode: 700
    - require:
      - pindy-dotfiles-workstation-common-gdm-config-file-accountsservice-dir-managed

pindy-dotfiles-workstation-common-gdm-config-file-accountsservice-icons-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService/icons
    - user: root
    - group: root
    - mode: 700
    - require:
      - pindy-dotfiles-workstation-common-gdm-config-file-accountsservice-dir-managed

pindy-dotfiles-workstation-common-gdm-config-file-user-config-managed:
  file.managed:
    - name: /var/lib/AccountsService/users/pindy
    - source: {{ files_switch(['gdm-user.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-gdm-config-file-user-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: root
    - group: root
    - template: jinja
    - context:
        gdm: {{ pindy_dotfiles_workstation_common.gdm | json }}
    - require:
      - pindy-dotfiles-workstation-common-gdm-config-file-accountsservice-users-dir-managed
