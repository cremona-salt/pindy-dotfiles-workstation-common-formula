# frozen_string_literal: true
#
control 'pindy_dotfiles_workstation_common face file' do
  title 'should match desired lines'

  describe file('/home/pindy/.face') do
    it { should be_file }
    it { should be_owned_by 'pindy' }
    it { should be_grouped_into 'pindy' }
    its('mode') { should cmp '0600' }
  end
end

control 'pindy_dotfiles_workstation_common face system version file' do
  title 'should match desired lines'

  describe file('/var/lib/AccountsService/icons/pindy') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
  end
end
