# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-face-config-file-face-absent:
  file.absent:
    - name: /home/pindy/.face

pindy-dotfiles-workstation-common-face-config-file-face-symlink-absent:
  file.absent:
    - name: /home/pindy/.face.icon
