# frozen_string_literal: true
#
control 'pindy_dotfiles_workstation_common face file' do
  title 'should be absent'

  describe file('/home/pindy/.face') do
    it { should_not exist }
  end
end

control 'pindy_dotfiles_workstation_common face system version file' do
  title 'should be absent'

  describe file('/var/lib/AccountsService/icons/pindy') do
    it { should_not exist }
  end
end
