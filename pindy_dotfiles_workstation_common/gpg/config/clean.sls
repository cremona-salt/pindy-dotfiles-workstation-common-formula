# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-gpg-config-file-gnupg-config-absent:
  file.absent:
    - name: /home/pindy/.gnupg/gpg.conf

pindy-dotfiles-workstation-common-gpg-config-file-gnupg-agent-absent:
  file.absent:
    - name: /home/pindy/.gnupg/gpg-agent.conf

pindy-dotfiles-workstation-common-gpg-config-file-gnupg-dir-absent:
  file.absent:
    - name: /home/pindy/.gnupg

