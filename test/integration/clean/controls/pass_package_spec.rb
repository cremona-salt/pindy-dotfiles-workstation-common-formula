# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common pass package' do
  title 'should not be installed'

  describe package('pass') do
    it { should_not be_installed }
  end
end

