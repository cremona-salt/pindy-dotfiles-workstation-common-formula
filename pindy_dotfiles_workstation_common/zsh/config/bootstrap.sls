# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-zsh-config-file-env-entry-config-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.env_entry_config }}
    - source: {{ files_switch(['zshenv-base.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-file-env-entry-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}

pindy-dotfiles-workstation-common-zsh-config-file-zdotdir-managed:
  file.directory:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}
    - user: pindy
    - group: pindy
    - mode: 755
    - makedirs: True

pindy-dotfiles-workstation-common-zsh-config-file-local-share-dir-managed:
  file.directory:
    - name: /home/pindy/.local/share/zsh
    - user: pindy
    - group: pindy
    - mode: 755
    - makedirs: True
