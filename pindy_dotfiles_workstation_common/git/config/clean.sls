# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-git-config-clean-git-dir-absent:
  file.absent:
    - name: /home/pindy/.config/git

pindy-dotfiles-workstation-common-git-config-clean-stock-gitfile-absent:
  file.absent:
    - name: /home/pindy/.gitconfig

{% set home = '/home/pindy' %}
{% set exclude_file = pindy_dotfiles_workstation_common.git.config.get("core.excludesFile", '~/.gitignore')|replace('~', home) %}
pindy-dotfiles-workstation-common-git-config-clean-gitignore-file-absent:
  file.absent:
    - name: {{ exclude_file }}
