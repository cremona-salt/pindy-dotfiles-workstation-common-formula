# frozen_string_literal: true
#
control 'pindy_dotfiles_workstation_common autostart config file' do
  title 'should be absent'

  describe file('/home/pindy/.config/autostart/gnome-keyring-ssh.desktop') do
    it { should_not exist }
  end
end

control 'pindy_dotfiles_workstation_common autostart config directory' do
  title 'should not exist'
  describe directory('/home/pindy/.config/autostart') do
    it { should_not exist }
  end
end
