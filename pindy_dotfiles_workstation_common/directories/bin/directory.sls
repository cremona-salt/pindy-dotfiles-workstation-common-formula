# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{#
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_user_present }}

#}
pindy-dotfiles-workstation-common-directories-bin-directory-present:
  file.directory:
    - name: {{ pindy_dotfiles_workstation_common.directories.bin_dir }}
    - user: pindy
    - group: pindy
    - mode: 755
    - makedirs: True
    {#
    - require:
      - sls: {{ sls_user_present }}
      #}
