# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_bootstrap = tplroot ~ '.zsh.config.bootstrap' %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_config_bootstrap }}

pindy-dotfiles-workstation-common-zsh-config-logout-config-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.logout_config }}
    - source: {{ files_switch(['zlogout.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-logout-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}
