# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}

pindy-dotfiles-workstation-common-zsh-config-clean-env-entry-config-file-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.env_entry_config }}

pindy-dotfiles-workstation-common-zsh-config-clean-env-config-file-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.env_config }}

pindy-dotfiles-workstation-common-zsh-config-clean-env-functions-file-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.env_functions }}

{%- set env_includes_dir = pindy_dotfiles_workstation_common.zsh.env_includes_dir %}
{% for name in pindy_dotfiles_workstation_common.zsh.env_include_files %}
pindy-dotfiles-workstation-common-zsh-config-clean-env-include-{{ name }}-managed:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ env_includes_dir }}/{{ name }}
{% endfor %}

pindy-dotfiles-workstation-common-zsh-config-clean-env-include-directory-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ env_includes_dir }}

pindy-dotfiles-workstation-common-zsh-config-clean-rc-config-file-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.rc_config }}

pindy-dotfiles-workstation-common-zsh-config-clean-profile-config-file-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.profile_config }}

pindy-dotfiles-workstation-common-zsh-config-clean-logout-config-file-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.logout_config }}

pindy-dotfiles-workstation-common-zsh-config-clean-zdotdir-directory-absent:
  file.absent:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}
