# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_bootstrap = tplroot ~ '.zsh.config.bootstrap' %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_config_bootstrap }}

pindy-dotfiles-workstation-common-zsh-config-env-config-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.env_config }}
    - source: {{ files_switch(['zshenv.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-env-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}

pindy-dotfiles-workstation-common-zsh-config-env-functions-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.env_functions }}
    - source: {{ files_switch(['zshenv_functions.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-env-functions-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}

{%- set env_includes_dir = pindy_dotfiles_workstation_common.zsh.env_includes_dir %}
pindy-dotfiles-workstation-common-zsh-config-env-include-dir-managed:
  file.directory:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ env_includes_dir }}
    - user: pindy
    - group: pindy
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_config_bootstrap }}

{% for name in pindy_dotfiles_workstation_common.zsh.env_include_files %}
pindy-dotfiles-workstation-common-zsh-config-env-include-{{ name }}-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ env_includes_dir }}/{{ name }}.zsh
    - source: {{ files_switch(['env_includes/' ~ name ~ '.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-env-include-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}
    - require:
      - pindy-dotfiles-workstation-common-zsh-config-env-include-dir-managed
{% endfor %}
