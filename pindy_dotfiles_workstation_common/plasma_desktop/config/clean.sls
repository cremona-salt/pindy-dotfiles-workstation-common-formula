# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set models = pindy_dotfiles_workstation_common.keyboard.models %}
{%- set layouts = pindy_dotfiles_workstation_common.keyboard.layouts %}
pindy-dotfiles-workstation-common-plasma-desktop-config-clean-kxkbrc-file-options-absent:
  ini.options_absent:
    - sections:
        Layout:
          Model: {{ models }}
          LayoutList: {{ layouts }}
