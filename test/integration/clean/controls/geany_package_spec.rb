# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common geany package' do
  title 'should not be installed'

  describe package('geany') do
    it { should_not be_installed }
  end
end

control 'pindy_dotfiles_workstation_common geany-plugins package' do
  title 'should not be installed'

  describe package('geany-plugins') do
    it { should_not be_installed }
  end
end
