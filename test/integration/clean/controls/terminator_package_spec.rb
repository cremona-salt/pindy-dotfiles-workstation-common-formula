# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common terminator package' do
  title 'should not be installed'

  describe package('terminator') do
    it { should_not be_installed }
  end
end
