# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common konsole config file' do
  title 'should match desired lines'

  describe file('/home/pindy/.config/kxkbrc') do
    it { should be_file }
    it { should be_owned_by 'pindy' }
    it { should be_grouped_into 'pindy' }
    its('mode') { should cmp '0600' }
    its('content') { should include('[Layout]') }
    its('content') { should include('LayoutList=us,gb') }
    its('content') { should include('Model=microsoft4000') }
  end
end

