# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-pam-environment-config-file-env-config-managed:
  file.managed:
    - name: /home/pindy/.pam_environment
    - source: {{ files_switch(['pam_environment.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-pam-environment-config-file-env-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: pindy
    - group: pindy
    - template: jinja
    - context:
        pam_environment: {{ pindy_dotfiles_workstation_common.pam_environment | json }}
