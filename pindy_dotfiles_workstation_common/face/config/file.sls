# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_gnome_config_file = tplroot ~ '.gdm.config.file' %}

include:
  - {{ sls_gnome_config_file }}

pindy-dotfiles-workstation-common-face-config-file-face-managed:
  file.managed:
    - name: /home/pindy/.face.icon
    - source: salt://pindy_dotfiles_workstation_common/face/files/default/face.png
    - mode: 600
    - user: pindy
    - group: pindy

pindy-dotfiles-workstation-common-face-config-file-face-system-version-managed:
  file.managed:
    - name: /var/lib/AccountsService/icons/pindy
    - source: salt://pindy_dotfiles_workstation_common/face/files/default/face.png
    - mode: 644
    - user: root
    - group: root
    - require:
      - sls: {{ sls_gnome_config_file }}
