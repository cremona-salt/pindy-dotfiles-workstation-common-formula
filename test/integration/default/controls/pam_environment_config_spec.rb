# frozen_string_literal: true
#
control 'pindy_dotfiles_workstation_common pam_environment config file' do
  title 'should match desired lines'

  describe file('/home/pindy/.pam_environment') do
    it { should be_file }
    it { should be_owned_by 'pindy' }
    it { should be_grouped_into 'pindy' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('SSH_AGENT_PID') }
    its('content') { should include('SSH_AUTH_SOCK') }
  end
end

