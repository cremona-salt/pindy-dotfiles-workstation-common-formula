# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common pass repo' do
  title 'should not exist'

  describe file('/home/pindy/.kitchen-pass-store') do
    it { should_not exist }
  end
end
