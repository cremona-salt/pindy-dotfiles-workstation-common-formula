# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{#
pindy-dotfiles-workstation-common-yubikey-config-file-udev-gnupg-ccid-rules-managed:
  file.managed:
    - name: /etc/udev/rules.d/71-gnupg-ccid.rules
    - source: {{ files_switch(['gnupg-ccid.rules.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-yubikey-config-file-udev-gnupg-ccid-rules-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    - context:
        yubikey: {{ pindy_dotfiles_workstation_common.yubikey | json }}
#}
