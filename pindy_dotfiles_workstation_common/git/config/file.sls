# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_user_present }}


pindy-dotfiles-workstation-common-git-config-file-dir-managed:
  file.directory:
    - name: /home/pindy/.config/git
    - user: pindy
    - group: pindy
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}

pindy-dotfiles-workstation-common-git-config-file-managed:
  file.managed:
    - name: /home/pindy/.config/git/config
    - source: {{ files_switch(['gitconfig.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-git-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - context:
        git: {{ pindy_dotfiles_workstation_common.git | json }}
    - require:
      - pindy-dotfiles-workstation-common-git-config-file-dir-managed
      - sls: {{ sls_user_present }}

{% set home = '/home/pindy' %}
{% set exclude_file = pindy_dotfiles_workstation_common.git.config.core.get("excludes_file", '~/.gitignore')|replace('~', home) %}
pindy-dotfiles-workstation-common-git-config-file-gitignore-managed:
  file.managed:
    - name: {{ exclude_file }}
    - source: {{ files_switch(['gitignore.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-git-config-file-gitignore-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - context:
        git: {{ pindy_dotfiles_workstation_common.git | json }}
    - require:
      - pindy-dotfiles-workstation-common-git-config-file-dir-managed
      - sls: {{ sls_user_present }}
