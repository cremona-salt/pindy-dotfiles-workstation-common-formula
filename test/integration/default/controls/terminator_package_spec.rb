# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common terminator package' do
  title 'should be installed'

  describe package('terminator') do
    it { should be_installed }
  end
end
