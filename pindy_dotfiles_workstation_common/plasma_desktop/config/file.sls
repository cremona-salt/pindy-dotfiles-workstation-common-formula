# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-plasma-desktop-config-file-kxkbrc-file-present:
  file.managed:
    - name: /home/pindy/.config/kxkbrc
    - mode: 600
    - user: pindy
    - group: pindy
    - makedirs: True
    - replace: False

{%- set models = pindy_dotfiles_workstation_common.keyboard.get('models', []) %}
{%- set layouts = pindy_dotfiles_workstation_common.keyboard.get('layouts', []) %}
pindy-dotfiles-workstation-common-plasma-desktop-config-file-kxkbrc-file-content-managed:
  ini.options_present:
    - name: /home/pindy/.config/kxkbrc
    - sections:
        Layout:
          Model: {{ models | join(',') }}
          LayoutList: {{ layouts | join(',') }}
    - require:
      - pindy-dotfiles-workstation-common-plasma-desktop-config-file-kxkbrc-file-present
