# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_bootstrap = tplroot ~ '.zsh.config.bootstrap' %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_config_bootstrap }}

pindy-dotfiles-workstation-common-zsh-config-rc-config-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.rc_config }}
    - source: {{ files_switch(['zshrc.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-rc-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}

{%- set rc_includes_dir = pindy_dotfiles_workstation_common.zsh.rc_includes_dir %}
pindy-dotfiles-workstation-common-zsh-config-rc-include-dir-managed:
  file.directory:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ rc_includes_dir }}
    - user: pindy
    - group: pindy
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_config_bootstrap }}

{% for name in pindy_dotfiles_workstation_common.zsh.rc_include_files %}
pindy-dotfiles-workstation-common-zsh-config-rc-include-{{ name }}-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ rc_includes_dir }}/{{ name }}.zsh
    - source: {{ files_switch(['rc_includes/' ~ name ~ '.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-rc-include-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}
    - require:
      - pindy-dotfiles-workstation-common-zsh-config-rc-include-dir-managed
{% endfor %}

pindy-dotfiles-workstation-common-zsh-config-plugins-managed:
  file.managed:
    - name: {{ pindy_dotfiles_workstation_common.zsh.zdotdir }}/{{ pindy_dotfiles_workstation_common.zsh.plugins_file }}
    - source: {{ files_switch(['plugins.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-zsh-config-plugins-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: pindy
    - group: pindy
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ pindy_dotfiles_workstation_common.zsh | json }}

