# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}

pindy-dotfiles-workstation-common-plasma-desktop-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ pindy_dotfiles_workstation_common.plasma_desktop.pkgs | yaml }}
