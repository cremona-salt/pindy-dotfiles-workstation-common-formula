# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common geany package' do
  title 'should be installed'

  describe package('geany') do
    it { should be_installed }
  end
end

control 'pindy_dotfiles_workstation_common geany-plugins package' do
  title 'should be installed'

  describe package('geany-plugins') do
    it { should be_installed }
  end
end
