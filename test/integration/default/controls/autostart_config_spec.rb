# frozen_string_literal: true
#
control 'pindy_dotfiles_workstation_common autostart config directory' do
  title 'should exist'

  describe directory('/home/pindy/.config/autostart') do
    it { should be_owned_by 'pindy' }
    it { should be_grouped_into 'pindy' }
    its('mode') { should cmp '0700' }
  end
end

control 'pindy_dotfiles_workstation_common autostart config file' do
  title 'should match desired lines'

  describe file('/home/pindy/.config/autostart/gnome-keyring-ssh.desktop') do
    it { should be_file }
    it { should be_owned_by 'pindy' }
    it { should be_grouped_into 'pindy' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[Desktop Entry]') }
    its('content') { should include('Type=Application') }
  end
end
