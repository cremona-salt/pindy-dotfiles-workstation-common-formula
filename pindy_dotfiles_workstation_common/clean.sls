# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .zsh.clean
  - .yubikey.clean
  - .xdg.clean
  - .terminator.clean
  - .plasma_desktop.clean
  - .pass.clean
  - .pam_environment.clean
  - .libreoffice.clean
  - .gpg.clean
  - .git.clean
  - .geany.clean
  - .gdm.clean
  - .face.clean
  - .directories.clean
  - .chromium.clean
  - .autostart.clean
  - .user.clean
