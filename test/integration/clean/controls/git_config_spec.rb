# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common git config dir' do
  title 'should not exist'

  describe directory('/home/pindy/.config/git') do
    it { should_not exist }
  end
end

control 'pindy_dotfiles_workstation_common git config file' do
  title 'should not exist'
  describe file('/home/pindy/.config/git/config') do
    it { should_not exist }
  end
end

control 'pindy_dotfiles_workstation_common git ignore file' do
  title 'should not exist'
  describe file('/home/pindy/.config/git/gitignore') do
    it { should_not exist }
  end
end
