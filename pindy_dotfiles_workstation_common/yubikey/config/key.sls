# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_gpg_package_install = tplroot ~ '.gpg.package.install' %}

include:
  - {{ sls_gpg_package_install }}

{%- for key in pindy_dotfiles_workstation_common.yubikey.get('trusted_keys', []) %}
{# This won't work until we have python3 salt #}
{#
pindy-dotfiles-workstation-common-yubikey-config-key-{{ key }}-added:
  gpg.present:
    - name: {{ key }}
    - keyserver: hkps://hkps.pool.sks-keyservers.net
    - user: pindy
    - trust: ultimately
    - gnupghome: /home/pindy/.gnupg
    - require:
      - sls: {{ sls_gpg_package_install }}
#}

pindy-dotfiles-workstation-common-yubikey-config-key-{{ key }}-manually-added:
  cmd.run:
    - name: gpg --keyserver hkps://keyserver.ubuntu.com --recv {{ key }} 
    - runas: pindy
    - unless: gpg --list-keys | grep {{ key }}

pindy-dotfiles-workstation-common-yubikey-config-key-{{ key }}-manually-trusted:
  cmd.run:
    - name: echo -e "5\ny\n" |  gpg --command-fd 0 --expert --edit-key {{ key }} trust
    - runas: pindy
    - onchanges:
      - cmd: pindy-dotfiles-workstation-common-yubikey-config-key-{{ key }}-manually-added

{%- endfor %}
