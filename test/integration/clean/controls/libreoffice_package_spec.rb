# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common libreoffice-fresh package' do
  title 'should not be installed'

  describe package('libreoffice-fresh') do
    it { should_not be_installed }
  end
end

control 'pindy_dotfiles_workstation_common libreoffice-fresh-en-gb package' do
  title 'should not be installed'

  describe package('libreoffice-fresh-en-gb') do
    it { should_not be_installed }
  end
end
