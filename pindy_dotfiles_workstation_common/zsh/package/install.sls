# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- set sls_bin_directory_create = tplroot ~ '.directories.bin.directory' %}
{%- set sls_local_share_directory_create = tplroot ~ '.directories.local_share.directory' %}

include:
  - {{ sls_bin_directory_create }}
  - {{ sls_local_share_directory_create }}

pindy-dotfiles-workstation-common-zsh-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ pindy_dotfiles_workstation_common.zsh.pkgs | yaml }}

{%- set antibody_version = pindy_dotfiles_workstation_common.zsh.antibody.version %}
pindy-dotfiles-workstation-common-zsh-package-install-antibody-installed:
  archive.extracted:
    - name: /home/pindy/.local/share/antibody
    - source: https://github.com/getantibody/antibody/releases/download/v{{ antibody_version }}/antibody_Linux_x86_64.tar.gz
    - source_hash: https://github.com/getantibody/antibody/releases/download/v{{ antibody_version }}/antibody_{{ antibody_version }}_checksums.txt
    - user: pindy
    - group: pindy
    - enforce_toplevel: False
    - require:
      - sls: {{ sls_local_share_directory_create }}

pindy-dotfiles-workstation-common-zsh-package-install-antibody-symlink-managed:
  file.symlink:
    - name: /home/pindy/bin/antibody
    - target: /home/pindy/.local/share/antibody/antibody
    - user: pindy
    - group: pindy
    - mode: 755
    - require:
      - pindy-dotfiles-workstation-common-zsh-package-install-antibody-installed
      - sls: {{ sls_bin_directory_create }}
