# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .user
  - .autostart
  - .chromium
  - .directories
  - .face
  - .gdm
  - .geany
  - .git
  - .gpg
  - .libreoffice
  - .pam_environment
  - .pass
  - .plasma_desktop
  - .terminator
  - .xdg
  - .yubikey
  - .zsh
