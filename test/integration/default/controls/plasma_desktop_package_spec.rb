# frozen_string_literal: true

packages = ['plasma-desktop', 'dolphin', 'dolphin-plugins', 'kcalc']

packages.each do |pkg|
  control 'pindy_dotfiles_workstation_common plasma-desktop ' + pkg + ' package' do
    title 'should be installed'
  
    describe package(pkg) do
      it { should be_installed }
    end
  end
end
