# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common pass repo' do
  title 'should be cloned'

  describe directory('/home/pindy/.kitchen-pass-store') do
    it { should be_owned_by 'pindy' }
    it { should be_grouped_into 'pindy' }
  end
end

control 'pindy_dotfiles_workstation_common pass config file' do
  title 'should match desired lines'

  describe file('/home/pindy/.kitchen-pass-store/FORMULA') do
    it { should be_file }
    it { should be_owned_by 'pindy' }
    it { should be_grouped_into 'pindy' }
    its('content') { should include('name: TEMPLATE') }
  end
end
