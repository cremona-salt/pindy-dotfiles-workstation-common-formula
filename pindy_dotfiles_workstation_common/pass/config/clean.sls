# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for store, data in pindy_dotfiles_workstation_common.pass.get('pass_stores', {}).items() %}
pindy-dotfiles-workstation-common-pass-config-repo-{{ store }}-pass-absent:
  file.absent:
    - name: {{ data.target }}

{%- endfor %}
