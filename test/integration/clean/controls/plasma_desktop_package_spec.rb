# frozen_string_literal: true

packages = ['plasma-desktop', 'dolphin', 'dolphin-plugins', 'kcalc']
packages.each do |pkg|
  control 'pindy_dotfiles_workstation_common plasma-desktop ' + pkg + ' package' do
    title 'should not be installed'
  
    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
