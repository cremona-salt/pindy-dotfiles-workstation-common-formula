# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common chromium package' do
  title 'should be installed'

  describe package('chromium') do
    it { should be_installed }
  end
end

control 'pindy_dotfiles_workstation_common browserpass-chromium package' do
  title 'should be installed'

  describe package('browserpass-chromium') do
    it { should be_installed }
  end
end

