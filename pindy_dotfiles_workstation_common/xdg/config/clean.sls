# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-conf-absent:
  file.absent:
    - name: /home/pindy/.config/user-dirs.dirs

pindy-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-locale-conf-absent:
  file.absent:
    - name: /home/pindy/.config/user-dirs.locale
