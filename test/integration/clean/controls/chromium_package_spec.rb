# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common chromium package' do
  title 'should not be installed'

  describe package('chromium') do
    it { should_not be_installed }
  end
end

control 'pindy_dotfiles_workstation_common browserpass-chromium package' do
  title 'should not be installed'

  describe package('browserpass-chromium') do
    it { should_not be_installed }
  end
end
