# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common pass package' do
  title 'should be installed'

  describe package('pass') do
    it { should be_installed }
  end
end

