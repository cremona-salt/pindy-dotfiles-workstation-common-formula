# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}


pindy-dotfiles-workstation-common-gpg-config-file-gnupg-dir-managed:
  file.directory:
    - name: /home/pindy/.gnupg
    - user: pindy
    - group: pindy
    - dir_mode: 700
    - file_mode: 600
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

pindy-dotfiles-workstation-common-gpg-config-file-gnupg-config-managed:
  file.managed:
    - name: /home/pindy/.gnupg/gpg.conf
    - source: {{ files_switch(['gpg.conf.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-gpg-config-file-gnupg-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: pindy
    - group: pindy
    - template: jinja
    - context:
        gpg: {{ pindy_dotfiles_workstation_common.gpg | json }}
    - require:
      - pindy-dotfiles-workstation-common-gpg-config-file-gnupg-dir-managed

pindy-dotfiles-workstation-common-gpg-config-file-gnupg-agent-managed:
  file.managed:
    - name: /home/pindy/.gnupg/gpg-agent.conf
    - source: {{ files_switch(['gpg-agent.conf.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-gpg-config-file-gnupg-agent-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: pindy
    - group: pindy
    - template: jinja
    - context:
        gpg: {{ pindy_dotfiles_workstation_common.gpg | json }}
    - require:
      - pindy-dotfiles-workstation-common-gpg-config-file-gnupg-dir-managed
