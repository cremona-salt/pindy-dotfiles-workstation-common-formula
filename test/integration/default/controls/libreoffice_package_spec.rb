# frozen_string_literal: true

control 'pindy_dotfiles_workstation_common libreoffice-fresh package' do
  title 'should be installed'

  describe package('libreoffice-fresh') do
    it { should be_installed }
  end
end

control 'pindy_dotfiles_workstation_common libreoffice-fresh-en-gb package' do
  title 'should be installed'

  describe package('libreoffice-fresh-en-gb') do
    it { should be_installed }
  end
end

