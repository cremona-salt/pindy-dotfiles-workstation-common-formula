# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import pindy_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

pindy-dotfiles-workstation-common-autostart-config-file-autostart-dir-managed:
  file.directory:
    - name: /home/pindy/.config/autostart
    - user: pindy
    - group: pindy
    - mode: 700
    - makedirs: True

pindy-dotfiles-workstation-common-autostart-config-file-autostart-gnome-keyring-ssh-managed:
  file.managed:
    - name: /home/pindy/.config/autostart/gnome-keyring-ssh.desktop
    - source: {{ files_switch(['gnome-keyring-ssh.desktop.tmpl'],
                              lookup='pindy-dotfiles-workstation-common-autostart-config-file-autostart-gnome-keyring-ssh-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: pindy
    - group: pindy
    - template: jinja
    - context:
        autostart: {{ pindy_dotfiles_workstation_common.autostart | json }}
    - require:
      - pindy-dotfiles-workstation-common-autostart-config-file-autostart-dir-managed
